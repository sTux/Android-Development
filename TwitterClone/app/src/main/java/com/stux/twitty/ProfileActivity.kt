package com.stux.twitty

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ProfileActivity : AppCompatActivity() {
    lateinit var userImage: ImageView
    lateinit var userName: TextView
    lateinit var userId: TextView

    /**
     * By default the update profile activity can be launched by clicking on the "Update Profile button
     * Or by clicking on the banner or Profile image
     * This common object is a implementation of a OnClickListener that starts the activity and used in all 3 views
     */
    private val switchToUpdate = View.OnClickListener {
        val updateProfileIntent = Intent(applicationContext, UpdateProfile::class.java)
        startActivity(updateProfileIntent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        /**
         * The action should hold only the back arrow and the menu burger button
         */
        supportActionBar?.title = ""


        userImage = findViewById(R.id.profileImage)
        userName = findViewById(R.id.profileName)
        userId = findViewById(R.id.profileUsername)

        findViewById<Button>(R.id.updateProfile).setOnClickListener(switchToUpdate)
        findViewById<FloatingActionButton>(R.id.composeTweet).setOnClickListener(START_TWEET_COMPOSE)

        userImage.setImageResource(R.mipmap.ic_launcher_round)
        userImage.setOnClickListener(switchToUpdate)

        userName.setText("Profile Name Placeholder")
        userId.setText("Profile username placeholder")
    }
}
