package com.stux.twitty

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<FloatingActionButton>(R.id.composeTweet).setOnClickListener(START_TWEET_COMPOSE)

        val profile = Intent(applicationContext, ProfileActivity::class.java)
        startActivity(profile)
    }
}
