package com.stux.twitty

import android.view.View
import android.widget.Toast


val START_TWEET_COMPOSE = View.OnClickListener { view ->
    val toast = Toast.makeText(view.context, "This will launch into compose tweet screen", Toast.LENGTH_LONG)
    toast.setGravity(10, 50, 50)
    toast.show()
}