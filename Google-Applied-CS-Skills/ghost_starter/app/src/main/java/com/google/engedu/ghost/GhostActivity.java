/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.engedu.ghost;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;


public class GhostActivity extends AppCompatActivity {
    private GhostDictionary dictionary;
    private boolean userTurn;
    private Random random = new Random();

    private Button challenge_btn = null;
    private TextView ghostText = null;
    private TextView gameStatus = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ghost);

        // Set-up references to all the TextViews and Buttons on the layout
        Button reset_btn = (Button) findViewById(R.id.reset);
        challenge_btn = (Button) findViewById(R.id.challenge);
        reset_btn.setOnClickListener(new View.OnClickListener() {
            // Implement and add OnClickListener here to prevent putting methods in the layout XML
            @Override
            public void onClick(View view) { turnSelector(); }
        });
        challenge_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { userChallenge(); }
        });
        gameStatus = (TextView) findViewById(R.id.gameStatus);
        ghostText = (TextView) findViewById(R.id.ghostText);

        AssetManager assetManager = getAssets();
        // Open with auto-closable stream and create a dictionary. If not able then show error toast
        try(InputStream file = assetManager.open("words.txt")) {
            dictionary = new SimpleDictionary(file);
        } catch (IOException err) {
            Toast error_toast = Toast.makeText(
                    getApplicationContext(), err.toString(), Toast.LENGTH_LONG
            );
            gameStatus.setText(err.getLocalizedMessage());
            error_toast.show();
        }

        turnSelector();
    }

    /**
     * Get the current word fragment
     * If it has at least 4 characters and is a valid word, declare victory for the user
     * otherwise if a word can be formed with the fragment as prefix, declare victory for the computer and display a possible word
     * If a word cannot be formed with the fragment, declare victory for the user
     **/
    private void userChallenge() {
        String word = ghostText.getText().toString();

        if (word.length() >= 4 && dictionary.isWord(word)) {
            Log.d(getLocalClassName(), "A valid word was formed by the computer");
            gameStatus.setText(R.string.user_won);
            challenge_btn.setClickable(false);
            return;
        }

        String possibility = dictionary.getAnyWordStartingWith(word);
        if (possibility == null) {
            Log.d(getLocalClassName(), "No valid word can be formed from the prefix");
            gameStatus.setText(R.string.user_won);
            challenge_btn.setClickable(false);
            return;
        }

        Log.d(getLocalClassName(), "The user challenged but word can be formed");
        gameStatus.setText(R.string.computer_won);
        ghostText.setText(possibility);
        challenge_btn.setClickable(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ghost, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Handler for the "Reset" button.
     * Randomly determines whether the game starts with a user turn or a computer turn.
     */
    public void turnSelector() {
        challenge_btn.setClickable(true);
        userTurn = random.nextBoolean();
        ghostText.setText("");

        if (userTurn) {
            gameStatus.setText(R.string.user_turn);
            Toast.makeText(getApplicationContext(), R.string.user_turn, Toast.LENGTH_LONG).show();
        } else {
            gameStatus.setText(R.string.computer_turn);
            computerTurn();
            Toast.makeText(getApplicationContext(), R.string.computer_turn, Toast.LENGTH_LONG).show();
        }
    }

    private void computerTurn() {
        // Do computer turn stuff then make it the user's turn again
        userTurn = true;
        gameStatus.setText(R.string.user_turn);
        Toast.makeText(getApplicationContext(), "Computer played", Toast.LENGTH_SHORT).show();
        String text = ghostText.getText().toString();

        // Check if the word fragment is at-least 4 characters long and form a valid word
        if (text.length() >= 4 && dictionary.isWord(text)) {
            Log.d(getLocalClassName(), "A valid word was formed by the user");
            gameStatus.setText(R.string.computer_won);
            ghostText.setText(text);
            challenge_btn.setClickable(false);
            return;
        }

        String result = dictionary.getAnyWordStartingWith(text);
        // No other word can be made with the present prefix. Declare victory
        if (result == null) {
            Log.d(getLocalClassName(), "No valid can be formed by the current prefix");
            gameStatus.setText(R.string.computer_won);
            challenge_btn.setClickable(false);
            return;
        }
        int position = text.length();
        ghostText.setText(String.format("%s%c", text, result.charAt(position)));
        Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
    }

    /**
     * Handler for user key presses.
     * @param keyCode: The value in event.getKeyCode().
     * @param event: Description of the key event
     * @return whether the key stroke was handled.
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        String input = String.format("%c", event.getUnicodeChar());
        boolean isAlphabetic = Character.isAlphabetic(input.codePointAt(0));

        if (userTurn && isAlphabetic) {
            String result = ghostText.getText() + input;
            ghostText.setText(result);

            if (dictionary.isWord(result)) {
                challenge_btn.setClickable(false);
                gameStatus.setText(R.string.computer_won);
            }
            userTurn = false;
            computerTurn();
        }
        return super.onKeyUp(keyCode, event);
    }
}
