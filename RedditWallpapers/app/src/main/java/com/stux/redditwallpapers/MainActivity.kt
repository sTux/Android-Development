package com.stux.redditwallpapers

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val wallpaper = Intent(this, Wallpaper::class.java)
//        startActivity(wallpaper)

        val posts = findViewById<RecyclerView>(R.id.posts).apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = PostsAdapter()
        }
    }
}
