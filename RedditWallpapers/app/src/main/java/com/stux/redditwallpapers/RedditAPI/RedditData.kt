package com.stux.redditwallpapers.RedditAPI


data class RedditData(val name: String) {
    override fun toString(): String {
        return name
    }
}