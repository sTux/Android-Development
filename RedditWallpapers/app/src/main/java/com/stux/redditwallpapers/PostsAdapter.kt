package com.stux.redditwallpapers

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class PostsAdapter: RecyclerView.Adapter<PostsAdapter.ViewHolder>() {
    private val TAG: String = "PostAdapter"

    class ViewHolder(postItem: View) : RecyclerView.ViewHolder(postItem) {
        var postImage: ImageView = postItem.findViewById(R.id.postImage)
        var postCaption: TextView = postItem.findViewById(R.id.postTitle)
        lateinit var postUrl: String
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layout = LayoutInflater.from(parent.context).inflate(R.layout.layout_wallpaper, parent, false)

        layout.setOnClickListener {
            val post = Intent(parent.context, Wallpaper::class.java)
            post.putExtra("POST_URL", "The actual post URL")
            parent.context.startActivity(post)
        }

        return ViewHolder(layout)
    }

    override fun getItemCount(): Int = Int.MAX_VALUE

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.postCaption.text = "Simple Text"
    }
}