package com.stux.redditwallpapers

import android.content.Intent
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity

import kotlinx.android.synthetic.main.activity_wallpaper.*

class Wallpaper : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallpaper)

        applyButton.setOnClickListener { view ->
            Snackbar.make(view, "Will apply the image", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        downloadButton.setOnClickListener { view ->
            Snackbar.make(view, "Will download the image to the app", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        Snackbar.make(downloadButton, intent.getStringExtra("POST_URL"), Snackbar.LENGTH_LONG).show()
    }
}
