package com.example.placeholderexcercise

import com.example.placeholderexcercise.JSONPlaceHolderAPI.APIAdapter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


val retrofit = with(Retrofit.Builder()) {
    baseUrl("https://jsonplaceholder.typicode.com/")
    addConverterFactory(GsonConverterFactory.create())
    build()
}

val api = retrofit.create(APIAdapter::class.java)