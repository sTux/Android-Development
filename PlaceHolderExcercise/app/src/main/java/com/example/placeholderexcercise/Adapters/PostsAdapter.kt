package com.example.placeholderexcercise.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.placeholderexcercise.R
import com.example.placeholderexcercise.JSONPlaceHolderAPI.PostsAdapter as Posts


class PostsAdapter(private val posts: List<Posts>, private val listener: PostOnClickListener): RecyclerView.Adapter<PostsAdapter.ViewHolder>() {
    interface PostOnClickListener {
        fun onClick(postId: Int);
    }

    class ViewHolder(post: View): RecyclerView.ViewHolder(post) {
        val title = post.findViewById<TextView>(R.id.title)
        val author = post.findViewById<TextView>(R.id.author)
        var postId: Int = 0
        lateinit var listener: PostOnClickListener

        init {
            post.setOnClickListener {
                listener.onClick(postId)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layout = LayoutInflater.from(parent.context).inflate(R.layout.posts_layout, parent, false)

        return ViewHolder(layout)
    }

    override fun getItemCount(): Int = posts.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.author.text = posts[position].user.toString()
        holder.title.text = posts[position].title
        holder.postId = posts[position].postId
        holder.listener = listener
    }
}