package com.example.placeholderexcercise

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.placeholderexcercise.Adapters.UsersAdapter
import com.example.placeholderexcercise.JSONPlaceHolderAPI.APIAdapter
import com.example.placeholderexcercise.JSONPlaceHolderAPI.UserAdapter
import com.google.android.material.snackbar.Snackbar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(), UsersAdapter.ItemOnClickListener {
    private val apiCallback = object: Callback<List<UserAdapter>> {
        override fun onFailure(call: Call<List<UserAdapter>>, t: Throwable) {
            // If the network call failed, then this will display a toast message with the error message
            Toast.makeText(this@MainActivity, t.message, Toast.LENGTH_LONG).show()
        }

        override fun onResponse(call: Call<List<UserAdapter>>, response: Response<List<UserAdapter>>) {
            /**
             * If the network call was not successful, then make a toast message notifying the user
             */
            if (!response.isSuccessful) {
                Snackbar.make(
                    this@MainActivity.findViewById(R.id.main_layout),
                    "Request failed",
                    Snackbar.LENGTH_LONG).show()

                return
            }

            // When the request is compete, create the recyclerview object and
            val recyclerView = this@MainActivity.findViewById<RecyclerView>(R.id.posts_holder)
            recyclerView.apply {
                layoutManager = LinearLayoutManager(this@MainActivity)
                adapter = UsersAdapter(response.body()!!, this@MainActivity)
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        api.users().apply {
            enqueue(apiCallback)
        }
    }

    override fun onClick(id: Int, name: String) {
        /**
         * The MainActivity implements the interface that glues the items onClick listener.
         * This method is called from the an item's onClick() and is passed with the user id associated with the item
         * Use that to start the new activity
         */
        with(Intent(applicationContext, UserPosts::class.java)) {
            putExtra("userId", id)
            putExtra("userName", name)
            this@MainActivity.startActivity(this)
        }
    }
}
