package com.example.placeholderexcercise.JSONPlaceHolderAPI

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface APIAdapter {
    @GET("users")
    fun users(): Call<List<UserAdapter>>

    @GET("users/{id}")
    fun user(@Path("id") id: Int): Call<UserAdapter>

    @GET("posts")
    fun posts(@Query("userId") userId: Int?): Call<List<PostsAdapter>>

    @GET("posts/{id}")
    fun post(@Path("id") postId: Int): Call<PostAdapter>
}