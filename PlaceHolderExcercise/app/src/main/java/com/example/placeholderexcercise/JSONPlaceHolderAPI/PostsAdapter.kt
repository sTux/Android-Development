package com.example.placeholderexcercise.JSONPlaceHolderAPI
import com.google.gson.annotations.SerializedName


data class PostsAdapter(
    @SerializedName("userId") val user: Int,
    @SerializedName("id") val postId: Int,
    @SerializedName("title") val title: String
) {
    override fun toString(): String {
        return "Post ${postId} by ${user}"
    }
}


data class PostAdapter(
    @SerializedName("userId") val user: Int,
    @SerializedName("id") val postId: Int,
    @SerializedName("title") val title: String,
    @SerializedName("body") val body: String
) {
    override fun toString(): String {
        return "Post ${postId} -> ${title}"
    }
}