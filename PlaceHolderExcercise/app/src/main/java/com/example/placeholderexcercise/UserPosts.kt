package com.example.placeholderexcercise

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.placeholderexcercise.Adapters.PostsAdapter
import com.example.placeholderexcercise.JSONPlaceHolderAPI.PostsAdapter as PostsAPI
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserPosts : AppCompatActivity(), PostsAdapter.PostOnClickListener {
    val apiCallBack = object: Callback<List<PostsAPI>> {
        override fun onFailure(call: Call<List<PostsAPI>>, t: Throwable) {
            Toast.makeText(applicationContext, t.localizedMessage, Toast.LENGTH_LONG).show()
        }

        override fun onResponse(call: Call<List<PostsAPI>>, response: Response<List<PostsAPI>>) {
            if (!response.isSuccessful) {
                Toast.makeText(applicationContext, response.code(), Toast.LENGTH_LONG).show()
                return
            }

            findViewById<RecyclerView>(R.id.postsHolder).apply {
                adapter = PostsAdapter(response.body()!!,this@UserPosts)
                layoutManager = LinearLayoutManager(this@UserPosts)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_posts)

        val userId: Int? = intent.extras?.get("userId") as Int?

        with(findViewById<TextView>(R.id.userName)) {
            text = intent.extras?.get("userName").toString()
        }

        api.posts(userId = userId).apply {
            enqueue(apiCallBack)
        }
    }

    override fun onClick(postId: Int) {
        Toast.makeText(this, "Will open up the post itself", Toast.LENGTH_LONG).show()
    }
}
