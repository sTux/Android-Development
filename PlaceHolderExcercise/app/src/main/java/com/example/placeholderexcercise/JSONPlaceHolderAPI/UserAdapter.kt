package com.example.placeholderexcercise.JSONPlaceHolderAPI
import com.google.gson.annotations.SerializedName


data class UserAdapter(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("username") val userName: String
)
