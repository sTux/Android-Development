package com.example.placeholderexcercise.Adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.placeholderexcercise.JSONPlaceHolderAPI.UserAdapter
import com.example.placeholderexcercise.R

class UsersAdapter(private val users: List<UserAdapter>, private val listener: ItemOnClickListener): RecyclerView.Adapter<UsersAdapter.ViewHolder>() {
    interface ItemOnClickListener {
        // This is the glue interface that glues the item's onClick listener to it's parent activity
        fun onClick(id: Int, name: String);
    }

    class ViewHolder(userView: View): RecyclerView.ViewHolder(userView) {
        lateinit var listener: ItemOnClickListener
        val name = userView.findViewById<TextView>(R.id.name)
        val username = userView.findViewById<TextView>(R.id.username)
        var userId = 0

        init {
            /**
             * A `init` block in Kotlin is a block of code that is run during the constructor phase
             * This block of code will be run, not matter which constructor has been invoked.
             * Here it just binds the view's onClick listener to the glue interface's on onClick method
             */
            userView.setOnClickListener {
                listener.onClick(userId, name.text.toString())
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layout = LayoutInflater.from(parent.context).inflate(R.layout.users_layout, parent, false)
        return ViewHolder(layout)
    }

    override fun getItemCount(): Int = users.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = users[position].name
        holder.username.text = "@ ${users[position].userName}"
        holder.userId = users[position].id
        holder.listener = listener
    }
}