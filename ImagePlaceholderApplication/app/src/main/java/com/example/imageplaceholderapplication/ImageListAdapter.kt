package com.example.imageplaceholderapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide


class ImageListAdapter(private val images: List<ImageItemAdapter>, private val handler: ListOnClickListener): RecyclerView.Adapter<ImageListAdapter.ImageViewHolder>() {
    interface ListOnClickListener {
        fun onClick(image: ImageProperty)
    }

    class ImageViewHolder(imageView: View): RecyclerView.ViewHolder(imageView) {
        var image: ImageView = imageView.findViewById(R.id.image)
        var author: TextView = imageView.findViewById(R.id.imageCaption)
        lateinit var property: ImageProperty
        lateinit var handler: ListOnClickListener

        // init block to run every-time during ViewHolder construction.
        // Sets a click listener for the view
        init {
            imageView.setOnClickListener {
                handler.onClick(property)
            }
        }
    }

    // Inflate the XML layout and create a ImageViewHolder object for the RecyclerView
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val inflated: View = LayoutInflater.from(parent.context)
                        .inflate(R.layout.layout_image_item, parent, false)

        return ImageViewHolder(inflated)
    }

    override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        with(holder) {
            // Set-up image
            author.text = images[position].author
            handler = this@ImageListAdapter.handler
            property = ImageProperty(
                images[position].id,
                images[position].height,
                images[position].width,
                images[position].author
            )

            Glide.with(holder.itemView.context)
                .load("https://picsum.photos/id/${property.id}/${property.width}/500/")
                .into(image)
        }
    }
}
