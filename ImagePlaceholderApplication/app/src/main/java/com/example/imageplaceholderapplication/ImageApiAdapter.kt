package com.example.imageplaceholderapplication

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ImageApiAdapter {
    @GET("list")
    fun list(@Query("page") page: Int?): Call<List<ImageItemAdapter>>

    @GET("id/{id}/{width}/{height}")
    fun image(@Path("id") id: Int, @Path("width") width: Int=500, @Path("height") height: Int=500): Call<ImageItemAdapter>
}