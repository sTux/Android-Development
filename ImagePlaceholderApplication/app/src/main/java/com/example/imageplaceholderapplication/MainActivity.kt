package com.example.imageplaceholderapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
//import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity(), ImageListAdapter.ListOnClickListener {
    val apiHandler = object: Callback<List<ImageItemAdapter>> {
        override fun onFailure(call: Call<List<ImageItemAdapter>>, t: Throwable) {
            Snackbar.make(
                this@MainActivity.findViewById(R.id.mainLayout),
                t.localizedMessage?: "",
                Snackbar.LENGTH_LONG
            ).show()
        }

        override fun onResponse(call: Call<List<ImageItemAdapter>>, response: Response<List<ImageItemAdapter>>) {
            if (response.isSuccessful) {
                // Find the RecyclerView and apply the layout manager and view adapter
                // N.B: The setting-up of the RecyclerView must be done on the main UI thread.
                // N.B: Reference - https://stackoverflow.com/questions/29141729/recyclerview-no-adapter-attached-skipping-layout
                this@MainActivity.findViewById<RecyclerView>(R.id.imageListHolder).apply {
                    layoutManager = LinearLayoutManager(this@MainActivity)
                    adapter = ImageListAdapter(response.body()!!, this@MainActivity)
                }

                return
            }

            Snackbar.make(
                this@MainActivity.findViewById(R.id.mainLayout),
                "Error parsing request",
                Snackbar.LENGTH_LONG
            ).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val retrofit = Retrofit.Builder()
            .baseUrl("https://www.picsum.photos/v2/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val api = retrofit.create(ImageApiAdapter::class.java)

        api.list(null).apply {
            enqueue(apiHandler)
        }
    }

    override fun onClick(image: ImageProperty) {
        with(Intent(applicationContext, ImageDetail::class.java)) {
            putExtra("image", image)

            startActivity(this)
        }
    }
}
