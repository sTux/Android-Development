package com.example.imageplaceholderapplication

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ImageProperty(val id: Int, val height: Int, val width: Int, val name: String) : Parcelable {
    fun dimensions(): String = "Height x Width = ${this.height} x ${this.width}"
}