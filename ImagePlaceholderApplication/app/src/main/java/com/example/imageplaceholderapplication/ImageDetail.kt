package com.example.imageplaceholderapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.bumptech.glide.Glide

class ImageDetail : AppCompatActivity() {
    private lateinit var imageProperty: ImageProperty

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_detail)
        this.imageProperty = intent.getParcelableExtra("image")!!

        findViewById<TextView>(R.id.image_dimensions).apply {
            text = this@ImageDetail.imageProperty.dimensions()
        }

        findViewById<TextView>(R.id.imageAuthor).apply {
            text = this@ImageDetail.imageProperty.name
        }

        Glide.with(this)
            .load("https://picsum.photos/id/${this.imageProperty.id}/${this.imageProperty.width}/${this.imageProperty.height}/")
            .into(findViewById(R.id.image))
    }
}
