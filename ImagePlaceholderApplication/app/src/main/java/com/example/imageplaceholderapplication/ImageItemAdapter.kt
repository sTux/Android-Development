package com.example.imageplaceholderapplication

import com.google.gson.annotations.SerializedName

data class ImageItemAdapter(
    @SerializedName("id") val id: Int,
    @SerializedName("width") val width: Int,
    @SerializedName("height") val height: Int,
    @SerializedName("download_url") val url: String,
    @SerializedName("author") val author: String
) {
    override fun toString(): String {
        return "Image id $id by $author (${width}x${height})"
    }
}