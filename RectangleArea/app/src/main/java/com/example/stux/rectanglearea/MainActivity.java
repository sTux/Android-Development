package com.example.stux.rectanglearea;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void calculate(View view) {
        EditText length_view = findViewById(R.id.length);
        EditText breadth_view = findViewById(R.id.breadth);
        TextView result = findViewById(R.id.result);


        Integer length = Integer.parseInt(length_view.getText().toString());
        Integer breadth = Integer.parseInt(breadth_view.getText().toString());
        double area = length.doubleValue() * breadth.doubleValue();

        result.setText(String.format(Locale.UK,"Area is %.3f", area));
    }

    public void clear(View view) {
        EditText length = findViewById(R.id.length);
        EditText breadth = findViewById(R.id.breadth);

        length.setText("");
        breadth.setText("");
    }
}
