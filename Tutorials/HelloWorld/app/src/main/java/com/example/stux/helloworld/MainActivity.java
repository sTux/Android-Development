package com.example.stux.helloworld;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.helloworld.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /* Called when the user taps the Send button */
    public void sendMessage(View view) {
        // Do something in response to button click

        // An Intent is an object that provides runtime binding between separate components, such as
        // two activities.
        // The Intent represents an app’s "intent to do something."
        // You can use intents for a wide variety of tasks, but in this lesson, your intent starts
        // another activity.
        // It's constructor takes two parameters:
        // A Context as its first parameter (this is used because the Activity class is a subclass)
        // The Class of the app component to which the system should deliver the Intent
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText editText = (EditText) findViewById(R.id.editText);  // Finds the element
        String message = editText.getText().toString();

        // The putExtra() method adds the EditText's value to the intent.
        // An Intent can carry data types as key-value pairs called extras.
        // Your key is a public constant EXTRA_MESSAGE because the next activity uses the key to
        // retrieve the text value.
        // It's a good practice to define keys for intent extras using your app's package name as a
        // prefix.
        // This ensures the keys are unique, in case your app interacts with other apps.
        intent.putExtra(EXTRA_MESSAGE, message);

        // The startActivity() method starts an instance of the DisplayMessageActivity specified by
        // the Intent.
        // Now you need to create that class.
        startActivity(intent);
    }
}
