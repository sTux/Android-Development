package com.example.internettutorial


import retrofit2.Call
import retrofit2.http.GET


interface PlaceholderAPI {
    @GET("/posts")
    fun posts(): Call<List<PostAdapter>>
}