package com.example.internettutorial

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.internettutorial.Posts.PostsAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val retrofit = with(Retrofit.Builder()) {
            baseUrl("https://jsonplaceholder.typicode.com/")
            addConverterFactory(GsonConverterFactory.create())
            build()
        }

        val api = retrofit.create(PlaceholderAPI::class.java);
        val call = api.posts();

        call.enqueue(object : Callback<List<PostAdapter>> {
            override fun onFailure(call: Call<List<PostAdapter>>, t: Throwable) {
                val view = this@MainActivity.findViewById<TextView>(R.id.text)
                view.text = t.message
            }

            override fun onResponse(call: Call<List<PostAdapter>>, response: Response<List<PostAdapter>>) {
                val view = this@MainActivity.findViewById<TextView>(R.id.text)

                if (response.isSuccessful) {
                    val posts = response.body()

                    val recyclerView = findViewById<RecyclerView>(R.id.posts)
                    recyclerView.apply {
                        adapter = PostsAdapter(posts!!)
                        layoutManager = LinearLayoutManager(this@MainActivity)
                    }
                } else
                    view.text = "Code " + response.code()
            }
        })
    }
}
