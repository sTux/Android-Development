package com.example.internettutorial

import com.google.gson.annotations.SerializedName


data class PostAdapter(
    var userId: Int,
    var postId: Int,
    var title: String,

    @SerializedName("body")
    var body: String
)