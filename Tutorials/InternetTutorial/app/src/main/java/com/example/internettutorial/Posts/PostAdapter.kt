package com.example.internettutorial.Posts

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.internettutorial.PostActivity
import com.example.internettutorial.PostAdapter
import com.example.internettutorial.R


class PostsAdapter(var posts: List<PostAdapter>): RecyclerView.Adapter<PostsAdapter.PostHolder>() {
    class PostHolder(postItem: View): RecyclerView.ViewHolder(postItem) {
        val title = postItem.findViewById<TextView>(R.id.title)
        var postId: Int = 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostHolder {
        val layout = LayoutInflater.from(parent.context).inflate(R.layout.posts_layout, parent, false)

        return PostHolder(layout)
    }

    override fun onBindViewHolder(holder: PostHolder, position: Int) {
        holder.postId = posts[position].postId
        holder.title.text = posts[position].title
    }

    override fun getItemCount(): Int = posts.size
}